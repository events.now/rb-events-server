# Events Server

A RESTful API Server for basic event-management (creating, joining, browsing events)

---

- [Events Server](#events-server)
  - [Use-Cases](#use-cases)
  - [Requirements](#requirements)
  - [Getting Started](#getting-started)
    - [Quickstart (TL;DR)](#quickstart-tldr)
    - [Installation](#installation)
      - [API-Server](#api-server)
      - [API Documentation](#api-documentation)
    - [Configurations](#configurations)
  - [Testing](#testing)
  - [Explore the API](#explore-the-api)
  - [FAQ](#faq)

---

## Use-Cases

1. **Event Creation**: a user can create an event by providing: name, location, start time, end time, and his/her e-mail (as a “host” of for an event)
2. **Listing of events**: a user can see the list of all events
3. **Management of events**: an event can be changed (name, location, start time/end time) or be deleted completely (thus deleting all sign ups to it too)
4. **Signing up**: a user can sign up for an event by providing an e-mail, he/she can only sign up with this e-mail once to the particular event
5. **Unsubscribe from an event**: a person can be unsubscribed from an event, by removing his/her e-mail from the event
6. **List all sign ups**: for a particular event, all sign ups / emails can be listed
7. **Email Notifications**: Email notifications are sent, when users sign up for events

## Requirements

- [Ruby](https://rvm.io/rvm/install) >= 2.6.3
- [Rails](https://guides.rubyonrails.org/v5.0/getting_started.html#installing-rails) >= 5.2.3
- [sqlite3](https://www.sqlite.org/quickstart.html) >= 3.24.0
- [Docker](https://docs.docker.com/install/) + [Docker-Compose](https://docs.docker.com/compose/install/) (for [OpenAPI-UI](#explore-the-api) and Quick Setup)

## Getting Started

A [Quickstart](#quickstart-tldr) setup is provided and runs the server in a docker-compose environment. If you prefer to run the server in a non-docker environment see [Installation](#installation)

### Quickstart (TL;DR)

1. Building the images

    building the events-server image

    ```bash
    # run inside top-level folder of this repo
    docker build -t events-server .
    ```

    building the openapi-ui image

    ```bash
    # run inside ./openapi/ folder
    docker build -t openapi-events-doc .
    ```

2. Starting the server

    Note: if you have tagged the images differently, you need to change them in *docker-compose.yml*

    ```bash
    # run inside top-level folder of this repo
    docker-compose up
    ```

    Docker compose starts:
    - the server: [localhost:3000](http://localhost:3000)
    - API Documentation: [localhost:8080](http://localhost:8080)
    - an local mail-server ([link](https://hub.docker.com/r/bytemark/smtp/)), for outgoing e-mails

3. [Explore the API](#explore-the-api)  
4. [Configuration](#configurations) of environment variables can be changed in docker-compose.yml

### Installation

#### API-Server

1. installing dependencies

    ```bash
    # run from top-level folder of this repo
    bundle install
    ```

1. prepare the database

    ```bash
    rails db:migrate
    ```

1. pre-fill db with initial data (optional)

   ```bash
   rails db:seed
   ```

1. start the server

   ```bash
   rails server
   ```

#### API Documentation

The API is specified with [OpenAPI v3](https://swagger.io/docs/specification/about/). An API-Documentation server can be started with *swagger-ui*.

1. Building the image

    building the openapi-ui image

    ```bash
    # run inside ./openapi/ folder
    docker build -t openapi-events-doc .
    ```

1. Starting API-Documentation server

    ```bash
    docker run --name api-doc -p 8080:8080 -it openapi-events-doc
    ```

    the API-Documentation is served at localhost:8080

### Configurations

The api server starts by default in *development* mode. Configurations for different environments can be changed directly in ./config/environments/ . For configuration of rails please consult [rails-documentation](https://guides.rubyonrails.org/configuring.html) .

The server-specific configurations are as follows and can be changed via environment variables:

| ENV  | Description | Default |
|---|---|---|
| API_KEY | Certain APIs are protected by this key. Expects an 'Authorization' Header and the Key in format: Token token="DefaultKey-12345-ABCD" | DefaultKey-12345-ABCD |
| SQL_FETCH_LIMIT  | Limits some SQL queries to return max. results  | 200 |
| HOST_EMAIL | Used when pre-filling the database with pre-defined values. Sets the host e-mail | foobar@example-email.com  |
| SMTP_ADDR  | Rails' SMTP Configuration: address  | localhost |
| SMTP_PORT  | Rails' SMTP Configuration: port  | 25  |
| SMTP_DOMAIN  | Rails' SMTP Configuration: domain  | my-domain.com  |
| SMTP_USER  | Rails' SMTP Configuration: user_name  | nil  |
| SMTP_PASSW  | Rails' SMTP Configuration: password  | nil  |
| SMTP_AUTH_OPT  | Rails' SMTP Configuration: authentication  | nil  |
| SMTP_STARTTLS_AUTO  | Rails' SMTP Configuration: enable_starttls_auto  | nil  |

## Testing

Run Unit tests with

```bash
# run in top-level folder of this repo
rails test
```

## Explore the API

Swagger-UI helps to explore the UI.

You can see all the specified APIs and also try-out the APIs on the spot.

![Try-out](https://gitlab.com/events.now/rb-events-server/uploads/7a99cfa4b5cdb17f5b5a50b75c6733e7/Screenshot_2019-08-08_at_01.33.48.png)

For protected APIs, you can click on the lock to authenticate.

![Authenticate](https://gitlab.com/events.now/rb-events-server/uploads/60175f592860d4be12f3eb865d2070da/Screenshot_2019-08-08_at_01.33.11.png)

## FAQ

1. How can I authenticate against API-Key protected APIs ?

    The server expects a Header named 'Authorization' and the value to be in the format Token token="XXXX". The default API-Key is DefaultKey-12345-ABCD so the value of the header should be:

    ```http
    Token token="DefaultKey-12345-ABCD"
    ```

2. I can not send e-mails from my local mail-server (e.g. [Quickstart](#quickstart-tldr) setup)

    There can be many causes, but most probably your public IP is denied by Anti-Spam Filters from the e-mail services.
