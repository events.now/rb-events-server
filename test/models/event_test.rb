require 'test_helper'
 
class EventTest < ActiveSupport::TestCase
  
    test "Should save valid Event" do
        ev = Event.new({
            name: "My New BD Party",
            location: "n/a",
            starttime: Time.now.utc.iso8601,
            endtime: Time.now.utc.iso8601,
            host: "my-email@some-email.com"
        })
        assert ev.save, "Failed to save valid Event"    
    end

    test "Should validate all required fields" do
        ev = Event.new({
            name: "foobar",
            location: "foobar",
            starttime: Time.now.utc.iso8601,
            endtime: Time.now.utc.iso8601,
            host: "foo@bar.com"
        })

        ev.host = nil
        assert_not ev.save, "Failed to require host property"
        ev.host = "foo@bar.com"

        ev.endtime = nil
        assert_not ev.save, "Failed to require endtime property"
        ev.endtime = Time.now.utc.iso8601

        ev.starttime = nil
        assert_not ev.save, "Failed to require starttime property"
        ev.starttime = Time.now.utc.iso8601

        ev.location = nil
        assert_not ev.save, "Failed to require location property"
        ev.location = "foobar"
        
        ev.name = nil
        assert_not ev.save, "Failed to require name property"
    end
  
    test "Should validate times" do
        ev = event(:birthdayEvent)
        ev.starttime = Time.now.utc.iso8601,
        ev.endtime = (Time.now .utc - 5 * 60 * 60).iso8601
        assert_not ev.save, "Saved an invalid end time"
    end

    test "Should validate e-mail format for host" do
        ev = event(:birthdayEvent)
        
        validEmails.each do |validEmails|
            ev.host = validEmails
            assert ev.save, "Did not saved with a valid e-mail address: #{ev.host}"
        end

        invalidEmails.each do |invalidEmail|
            ev.host = invalidEmails
            assert_not ev.save, "Saved an invalid e-mail address: #{ev.host}"
        end
    end

    test "Should not conflict with unique name&host constraint" do
        ev = event(:bbqEvent)
        
        # same name and host
        ev2 = Event.new({
            name: ev.name,
            location: "dont care",
            starttime: "2019-08-08T12:12:12.000Z",
            endtime: "2019-08-08T16:12:12.000Z",
            host: ev.host
        })
        assert_raise ActiveRecord::RecordNotUnique, "Unique constraint: name,host broken" do 
            ev2.save
        end

        # change to new name
        ev2.name = "My NEW BBQ Party"
        assert ev2.save, "Failed to save valid Event"
        
        # updates to same new name
        assert_raise ActiveRecord::RecordNotUnique, "Unique constraint: name,host broken" do 
            ev.update({name: "My NEW BBQ Party"})
        end
    end
end