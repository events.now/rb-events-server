require 'test_helper'
 
class SignupTest < ActiveSupport::TestCase
  
    test "Should save valid Signup" do
        ev = event(:birthdayEvent)
        su = Signup.new({
            events_id: ev.id,
            email: "my-email@some-email.com"
        })
        assert su.save, "Failed to save valid Signup"    
    end

    test "Should validate all required fields" do
        ev = event(:birthdayEvent)
        su = Signup.new({
            events_id: ev.id,
            email: "123-asd-123@sdfdsfds.com"
        })

        su.events_id = nil
        assert_not su.save, "Failed to require events_id"
        su.events_id = ev.id

        su.email = nil
        assert_not su.save, "Failed to require email property"
    end

    test "Should validate e-mail format" do
        ev = event(:birthdayEvent)
        
        su = Signup.new({
            events_id: ev.id,
            email: "my-email@some-email.com"
        })

        validEmails.each do |validEmails|
            su.email = validEmails
            assert su.save, "Did not saved with a valid e-mail address: #{su.email}"
        end

        invalidEmails.each do |invalidEmail|
            su.email = invalidEmails
            assert_not su.save, "Saved an invalid e-mail address: #{su.email}"
        end
    end

    test "Should only signup once per email" do
        ev = event(:bbqEvent)
        su1 = Signup.new({
            events_id: ev.id,
            email: "first@email.com"
        })

        su2 = Signup.new({
            events_id: ev.id,
            email: "first@email.com"
        })

        su3 = Signup.new({
            events_id: ev.id,
            email: "new-valid@email.com"
        })
        assert_nothing_raised do su1.save end
        assert_raise ActiveRecord::RecordNotUnique, "Saved email more than once for same event" do su2.save end
        assert_nothing_raised do su3.save end
    end
end