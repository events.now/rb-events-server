FROM ruby:2.6.3

# update bundler
RUN gem update bundler

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

CMD rails db:migrate && rails db:seed && rails s -b 0.0.0.0