# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ocassions = ["Birthday Party", "Graduation", "Shopping Day", "BBQ Party", "Wedding", "Funeral", "Movie Day"]

ocassions.each do |o|
    event = Event.create({
        name: "#{Faker::Movies::LordOfTheRings.character}'s #{o}",
        location: "#{Faker::Address.full_address}",
        host: Rails.application.config.host_email,
        starttime: Time.now.utc.iso8601,
        endtime: (Time.now .utc + 5 * 60 * 60).iso8601
    })
    if event.errors.any?
        puts "Could not create event #{event.name} errors: #{event.errors.full_messages}"
    end

    signup = Signup.create({
        events_id: event.id,
        email: "#{Faker::Name.first_name}.#{Faker::Name.last_name}@some-company.com",
        message: "automatically signed up by routine"
    })

    if signup.errors.any?
        puts "Could not create signup for #{event.id} errors: #{signup.errors.full_messages}"
    end
end