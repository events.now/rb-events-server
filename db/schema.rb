# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_05_004833) do

  create_table "events", force: :cascade do |t|
    t.string "name", null: false
    t.text "location", null: false
    t.datetime "starttime", null: false
    t.datetime "endtime", null: false
    t.string "host", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "host"], name: "index_events_on_name_and_host", unique: true
  end

  create_table "signups", force: :cascade do |t|
    t.integer "events_id"
    t.string "email", null: false
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["events_id", "email"], name: "index_signups_on_events_id_and_email", unique: true
    t.index ["events_id"], name: "index_signups_on_events_id"
  end

end
