class CreateEventAndSignUpTables < ActiveRecord::Migration[5.2]
  def change

    create_table :events do |t|
      t.string :name, null: false
      t.text :location, null: false
      t.datetime :starttime, null: false
      t.datetime :endtime, null: false
      t.string :host, null: false
      t.text :description
      t.timestamps
    end
    
    add_index :events, [:name, :host], unique: true

    create_table :signups do |t|
      t.references :events, foreign_key: {on_delete: :cascade}
      t.string :email, null: false
      t.text :message
      t.timestamps
    end

    add_index :signups, [:events_id, :email], unique: true
  end
end
