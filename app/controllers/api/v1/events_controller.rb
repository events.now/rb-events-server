module Api
    module V1
        class EventsController < ApplicationController
            
            # Lists all events, limited by threshold
            def index
                begin
                    events = Event.all.limit(Rails.application.config.sql_fetch_limit)
                    render json: events, status: 200
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to list events"}, status: 500
                end
            end

            # Loads a specific event by id
            def show
                begin
                    event = Event.find(params[:id])
                    render json: event, status: 200
                rescue ActiveRecord::RecordNotFound => e
                    render json: { message: e.message, error: "Record does not exist: #{params[:id]}"}, status: 404
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to load event: #{params[:id]}"}, status: 500
                end
            end

            # saves an event to db
            def create
                begin
                    event = Event.create(createParams)
                    if event.errors.any?
                        render json: { message: event.errors.full_messages.to_sentence, error: "invalid event"}, status: 400
                    else
                        render json: event, status: 200
                    end
                rescue ActiveRecord::RecordNotUnique => e
                    render json: { message: "An event with the same name has already been created by host", error: "no same event-name from same host"}, status: 409
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to store event"}, status: 500
                end
            end

            # updates an event (merges changes from input into record)
            def update
                begin
                    event = Event.find(params[:id])
                    event.update(updateParams)
                    if event.errors.any?
                        render json: { message: event.errors.full_messages.to_sentence, error: "failed to update event"}, status: 400
                    else
                        render json: { message: "OK" }, status: 200
                    end
                rescue ActiveRecord::RecordNotFound => e
                    render json: { message: e.message, error: "Record does not exist: #{params[:id]}"}, status: 404
                rescue ActiveRecord::RecordNotUnique => e
                    render json: { message: "the event with the same name has already been created by host", error: "no same event-name from same host"}, status: 409
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to update event"}, status: 500
                end
            end

            # deletes an event by id
            def destroy
                begin
                    event = Event.find(params[:id]).destroy
                    render json: { message: "OK" }, status: 200
                rescue ActiveRecord::RecordNotFound => e
                    render json: { message: e.message, error: "Record does not exist: #{params[:id]}"}, status: 404
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to delete event: #{params[:id]}"}, status: 500
                end
            end

            private def createParams
                params.permit(:name, :location, :starttime, :endtime, :host)
            end

            private def updateParams
                # does not allow to change the host
                params.permit(:name, :location, :starttime, :endtime)
            end
        end
    end
end