module Api
    module V1
        class SignupsController < ApplicationController
            include ActionController::HttpAuthentication::Token::ControllerMethods

            before_action :authenticate

            # Authentication with Token, checks Header 'Authorization' for value Token token="abc"'
            # https://api.rubyonrails.org/classes/ActionController/HttpAuthentication/Token.html
            def authenticate
                authenticated = authenticate_with_http_token do |token, options|
                    token == Rails.application.config.api_key
                end

                if !authenticated
                    render json: { message: "Bad credentials", "error": "Not Authorized"}, status: 401
                end         
            end

            # Loads all sign ups from a given event (by event_id)
            def show
                begin
                    signups = Signup.where(events_id: params[:event_id]).limit(Rails.application.config.sql_fetch_limit)
                    render json: signups, status: 200
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to list signups"}, status: 500
                end
            end

            # sign up an email to an event (specified by event_id)
            def create
                begin
                    signup = Signup.create({ :events_id => params[:event_id], :email => params[:email] })
                    if signup.errors.any?
                        render json: { message: signup.errors.full_messages.to_sentence, error: "invalid signup"}, status: 400
                    else
                        event = Event.find(params[:event_id])
                        SignupMailer.with(email: params[:email], event: event).signup_email.deliver_later
                        CalendarInviteMailer.with(email: params[:email], event: event).calendar_email.deliver_later
                        render json: signup, status: 200
                    end
                rescue ActiveRecord::InvalidForeignKey => e
                    render json: { message: "Could not find event: #{params[:event_id]}", error: "event does not exist" }, status: 409
                rescue ActiveRecord::RecordNotUnique => e
                    render json: { message: "#{params[:email]} has already signed up to event", error: "email already exists"}, status: 409
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to store signup"}, status: 500
                end
            end

            # unsubscribe from an event (specified by event_id) by email (with query: ?email)
            def destroy
                begin
                    if !request.query_parameters[:email] || request.query_parameters[:email].empty?
                        return render json: { message: "please provide an email in order to unsubscribe from this event: #{params[:event_id]} ", error: "invalid input"}, status: 400 
                    end

                    signup = Signup.where(email: params[:email], events_id: params[:event_id]).delete_all
                    if signup > 0
                        render json: { message: "OK" }, status: 200
                    else
                        render json: { message: "#{params[:email]} did not sign up for event: #{params[:event_id]}", error: "signup record does not exist "}, status: 404
                    end
                rescue ActiveRecord::ActiveRecordError => e
                    render json: { message: e.message, error: "Failed to delete signup: #{params[:id]}"}, status: 500
                end
            end
        end
    end
end