class Event < ApplicationRecord
    validates :name, presence: true
    validates :location, presence: true
    validates :starttime, presence: true
    validates :endtime, presence: true
    validates :host, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
    validate :endtime_not_before_starttime, on: [:create, :update]
 
    # validates that endtime is not before starttime
    def endtime_not_before_starttime
        if endtime.present? && starttime.present? && endtime < starttime
            errors.add(:endtime, "can't be before starttime")
        end
    end
end