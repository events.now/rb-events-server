class SignupMailer < ApplicationMailer 
  def signup_email
    @event = params[:event]
    @email = params[:email]
    mail(to: @event.host, subject: "Signup notification for your event: #{@event.name}")
  end
end
