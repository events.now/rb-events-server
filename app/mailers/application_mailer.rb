class ApplicationMailer < ActionMailer::Base
  default from: "do-not-reply@#{ENV.fetch("SMTP_DOMAIN", "my-domain.com")}"
  layout 'mailer'
end
