class CalendarInviteMailer < ApplicationMailer
  def calendar_email
    @email = params[:email]
    @event = params[:event]
    
    ical = Icalendar::Calendar.new         
    e = Icalendar::Event.new    
    e.dtstart = Icalendar::Values::DateTime.new @event.starttime
    e.dtend   = Icalendar::Values::DateTime.new @event.endtime
    e.organizer = @event.host
    e.attendee  = @email   
    e.location = @event.location     
    e.summary = "Reminder: #{@event.name}"   
    e.description = "You have signed up to event: #{@event.name}"
    ical.add_event(e)    
    ical.append_custom_property('METHOD', 'REQUEST')
    mail.attachments.inline['calendar_invitation.ics'] = { :mime_type => 'text/calendar', content: ical.to_ical }
    mail(to: @email, subject: 'Calendar invitation')
  end
end
